const path = require("path");
const csvToJson = require("csvtojson");
const fs = require("fs");

const {
  matchesPerYear,
  matchesWonPerTeam,
  extraRunsIn2016,
  mostEconomicalBowlers,
  teamWinningBothTossAndMatch,
  playerOfTheMatchPerSeason,
  strikeRate,
  highestNumberOfTimesPlayerDismissed,
  bestEconomicalInSuperOvers,
  numberOfBoundariesDhoniScored,
  boundariesByKohli,
  playerOfTheMatch,
  purpleCapHolder,
  mostWinsAgainstMumbai,
  orangeCapHolder,
  dhoniVsMalinga,
  rainaVsBangalore,
  partnershipBwnGayleAndKohli,
} = require("./ipl");

const BASE_PATH = path.join(__dirname, "../");
const MATCHES_FILE_PATH = path.join(BASE_PATH, "./data/matches.csv");
const DELIVERIES_FILE_PATH = path.join(BASE_PATH, "./data/deliveries.csv");
/**
 * The function converts the CSV file to JSON file with csvToJson module.
 * @returns {undefined} - It won't return anything, instead it will store the data to use it in call back function.
 */
function index() {
  csvToJson()
    .fromFile(MATCHES_FILE_PATH)
    .then((matches) => {
      const result = matchesPerYear(matches);
      const matchesWon = matchesWonPerTeam(matches);
      const tossAndMatch = teamWinningBothTossAndMatch(matches);
      const playerOfMatch = playerOfTheMatchPerSeason(matches);
      const winsAgainstMumbai = mostWinsAgainstMumbai(matches);
      csvToJson()
        .fromFile(DELIVERIES_FILE_PATH)
        .then((deliveries) => {
          const runs = extraRunsIn2016(matches, deliveries);
          const economy = mostEconomicalBowlers(matches, deliveries);
          const batsmanStrikeRate = strikeRate(matches, deliveries);
          const dismissedBatsman =
            highestNumberOfTimesPlayerDismissed(deliveries);
          const bestBowlersInSuperOvers =
            bestEconomicalInSuperOvers(deliveries);
          const boundariesOfDhoni = numberOfBoundariesDhoniScored(
            matches,
            deliveries
          );
          const boundariesOfKohli = boundariesByKohli(matches, deliveries);
          const playerOfMatchIn2010 = playerOfTheMatch(matches, deliveries);
          const mostWickets = purpleCapHolder(matches, deliveries);
          const mostRuns = orangeCapHolder(matches, deliveries);
          const boundariesToMalinga = dhoniVsMalinga(matches, deliveries);
          const rainaRuns = rainaVsBangalore(matches, deliveries);
          const partnerShip = partnershipBwnGayleAndKohli(matches, deliveries);
          outputData(
            result,
            matchesWon,
            runs,
            economy,
            tossAndMatch,
            playerOfMatch,
            batsmanStrikeRate,
            dismissedBatsman,
            bestBowlersInSuperOvers,
            boundariesOfDhoni,
            boundariesOfKohli,
            playerOfMatchIn2010,
            mostWickets,
            winsAgainstMumbai,
            mostRuns,
            boundariesToMalinga,
            rainaRuns,
            partnerShip
          );
        });
    })
    .catch(console.log);
}
/**
 * The function accept all the json files.
 * Write those in the specific path given.
 * @param {JSON} results - The json file having number of matches per year.
 * @param {JSON} wonMatch - The json file having matches won per team per year.
 * @param {JSON} run - The json file having extra runs by each team in the the year 2016.
 * @param {JSON} economy - The json file having top 10 economical bowlers.
 * @param {JSON} tossAndMatch - The json file having number of times each team winning both toss and match.
 * @param {JSON} playerOfMatch - The json file having highest number of times a player go the player of the match for every season.
 * @param {JSON} srOfBatsman - The json file having strike rate of each batsman per season.
 * @param {JSON} dismissedBatsman - The json file having number of times each batsmen dismissed.
 * @param {JSON} bestBowlersInSuperOvers - The json file having best economical bowlers in the super overs.
 * @param {JSON} boundariesOfDhoni - The json file having the number of boundaries by dhoni per each season.
 * @returns {undefined} - It won't return anything in this file.
 */
const outputData = (
  results,
  wonMatch,
  run,
  economy,
  tossAndMatch,
  playerOfMatch,
  srOfBatsman,
  dismissedBatsman,
  bestBowlersInSuperOvers,
  boundariesOfDhoni,
  boundariesOfKohli,
  player,
  purpleCap,
  againstMumbai,
  orangeCap,
  boundary,
  raina,
  partner
) => {
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/matchesPerYear.json"),
    JSON.stringify(results),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/matchesWonPerTeamPerYear.json"),
    JSON.stringify(wonMatch),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/extraRunsIn2016.json"),
    JSON.stringify(run),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/mostEconomicalBowlers.json"),
    JSON.stringify(economy),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/teamWinningBothTossAndMatch.json"),
    JSON.stringify(tossAndMatch),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/playerOfTheMatchPerYear.json"),
    JSON.stringify(playerOfMatch),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(
      BASE_PATH,
      "./public/output/strikeRateOfEachBatsmanPerSeason.json"
    ),
    JSON.stringify(srOfBatsman),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(
      BASE_PATH,
      "./public/output/highestNoOfTimesPlayerDismissed.json"
    ),
    JSON.stringify(dismissedBatsman),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(
      BASE_PATH,
      "./public/output/besteconomicalBowlersInSuperOvers.json"
    ),
    JSON.stringify(bestBowlersInSuperOvers),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/dhoniBoundaries.json"),
    JSON.stringify(boundariesOfDhoni),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/kohliBoundaries.json"),
    JSON.stringify(boundariesOfKohli),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/playerOfMatchIn2010.json"),
    JSON.stringify(player),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/purpleCapHolder.json"),
    JSON.stringify(purpleCap),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/winsAgainstMumbai.json"),
    JSON.stringify(againstMumbai),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/orangeCapHolder.json"),
    JSON.stringify(orangeCap),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/dhoniVsMalinga.json"),
    JSON.stringify(boundary),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/rainaVsRCB.json"),
    JSON.stringify(raina),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
  fs.writeFile(
    path.join(BASE_PATH, "./public/output/partnershipBwnGayleAndKohli.json"),
    JSON.stringify(partner),
    "utf8",
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
};

index();
