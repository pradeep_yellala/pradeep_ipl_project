/**
 * This function accepts the details about ipl matches.
 * Returns an object of year as property and number of matches in that season as value to that property.
 * @param {Array} matches - The array of objects.
 * @returns {Object} - returns an object.
 */

function matchesPerYear(matches) {
  const matchesPerSeason = matches.reduce((accumulator, { season }) => {
    if (accumulator[season] !== undefined) {
      accumulator[season] += 1;
    } else {
      accumulator[season] = 1;
    }
    return accumulator;
  }, {});
  return matchesPerSeason;
}

/**
 * This function accepts the details about ipl matches.
 * Returns an object of year as property and object as values to that property.
 * Value object contains team name as property and number of won in that season as value.
 * @param {Array} matches - The arrays of objects.
 * @returns {object} - Returns an object.
 */

const matchesWonPerTeam = (matches) => {
  const matchesWonPerTeamPerYear = matches.reduce(
    (accumulator, { season, winner }) => {
      if (accumulator[season] !== undefined) {
        if (accumulator[season][winner] !== undefined) {
          accumulator[season][winner] += 1;
        } else {
          accumulator[season][winner] = 1;
        }
      } else {
        accumulator[season] = {};
        accumulator[season][winner] = 1;
      }
      return accumulator;
    },
    {}
  );
  return matchesWonPerTeamPerYear;
};
/**
 * The function accepts the details about ipl matches and ball to ball analysis.
 * Returns an object of which team name as property and extra runs conceded by each team as value to its property.
 * @param {Array} matches - The array of objects.
 * @param {Array} deliveries - The array of objects.
 * @param {Number} year - The year of analysis.
 * @returns {Object} - Returns an object.
 */

const extraRunsIn2016 = (matches, deliveries, year = 2016) => {
  const filteredDeliveries = matches
    .filter(({ season }) => season == year)
    .reduce((accumulator, { id }) => {
      accumulator.push(...deliveries.filter(({ match_id }) => id == match_id));
      return accumulator;
    }, []);

  const extraRuns = filteredDeliveries.reduce(
    (accumulator, { bowling_team, extra_runs }) => {
      if (accumulator[bowling_team]) {
        accumulator[bowling_team] += Number(extra_runs);
      } else {
        accumulator[bowling_team] = Number(extra_runs);
      }
      return accumulator;
    },
    {}
  );

  return extraRuns;
};
/**
 * The function accepts the details about ipl matches and ball to ball analysis.
 * Returns an object of which player name as property and their economy as value.
 * @param {Array} matches - The array of objects.
 * @param {Array} deliveries - The array of objects.
 * @param {Number} year -  The year of analysis.
 * @returns {Object} - Returns an object.
 */

const mostEconomicalBowlers = (matches, deliveries, year = 2015) => {
  const idList = matches
    .filter(({ season }) => season == year)
    .reduce((accumulator, { id }) => {
      accumulator.push(...deliveries.filter(({ match_id }) => match_id === id));
      return accumulator;
    }, []);
  const bowlersList = idList.reduce((accumulator, { batsman_runs, bowler }) => {
    if (accumulator[bowler]) {
      accumulator[bowler]["runs"] += Number(batsman_runs);
      accumulator[bowler]["balls"] += 1;
    } else {
      accumulator[bowler] = {};
      accumulator[bowler]["runs"] = Number(batsman_runs);
      accumulator[bowler]["balls"] = 1;
    }
    return accumulator;
  }, {});
  const topEconomicalBowlers = Object.keys(bowlersList).reduce(
    (accumulator, bowler) => {
      const runs = bowlersList[bowler]["runs"];
      const overs = bowlersList[bowler]["balls"] / 6;
      accumulator.push([bowler, runs / overs]);
      return accumulator;
    },
    []
  );
  const sortedEconomicalBowlers = topEconomicalBowlers
    .sort((a, b) => a[1] - b[1])
    .slice(0, 10)
    .reduce((accumulator, bowler) => {
      accumulator[bowler[0]] = bowler[1].toFixed(2);
      return accumulator;
    }, {});
  return sortedEconomicalBowlers;
};
/**
 * The function accepts the details about ipl matches.
 * Returns an object of which team name as property and number as value.
 * @param {Array} matches - The array of objects.
 * @returns {Object} - Returns an object.
 */

const teamWinningBothTossAndMatch = (matches) => {
  const winningBothTossAndMatch = matches
    .filter(({ toss_winner, winner }) => toss_winner === winner)
    .reduce((accumulator, { toss_winner }) => {
      if (accumulator[toss_winner]) {
        accumulator[toss_winner] += 1;
      } else {
        accumulator[toss_winner] = 1;
      }
      return accumulator;
    }, {});

  return winningBothTossAndMatch;
};
/**
 * The function accepts the details about ipl matches.
 * Returns an object of which year as a property and an object as the value.
 * The value object contains player name as property and number of times won the player of the match award as value.
 * @param {Array} matches - The array of objects.
 * @returns {Object} - Returns an object.
 */

const playerOfTheMatchPerSeason = (matches) => {
  const playerOfMatch = matches.reduce(
    (accumulator, { season, player_of_match }) => {
      if (accumulator[season]) {
        if (accumulator[season][player_of_match]) {
          accumulator[season][player_of_match] += 1;
        } else {
          accumulator[season][player_of_match] = 1;
        }
      } else {
        accumulator[season] = {};
        accumulator[season][player_of_match] = 1;
      }
      return accumulator;
    },
    {}
  );
  const highestNumber = Object.keys(playerOfMatch).reduce(
    (accumulator, season) => {
      let player = Object.keys(playerOfMatch[season]).reduce((a, b) =>
        playerOfMatch[season][a] > playerOfMatch[season][b] ? a : b
      );
      accumulator[season] = { [player]: playerOfMatch[season][player] };
      return accumulator;
    },
    {}
  );
  return highestNumber;
};
/**
 * The function accepts the details about ipl matches and ball to ball analysis.
 * Returns an object of which year as property and an object as value.
 * The value object contains player name as property and his strike rate as value.
 * @param {Array} matches - The array of objects.
 * @param {Array} deliveries - The array of objects.
 * @returns {Object} - Returns an object.
 */

const strikeRate = (matches, deliveries) => {
  const runsAndBallsPerSeason = matches.reduce(
    (accumulator, { season, id }) => {
      deliveries
        .filter(({ match_id }) => match_id === id)
        .map(({ batsman, batsman_runs }) => {
          if (accumulator[season]) {
            if (accumulator[season][batsman]) {
              accumulator[season][batsman]["balls"] += 1;
              accumulator[season][batsman]["total_runs"] +=
                Number(batsman_runs);
            } else {
              accumulator[season][batsman] = {};
              accumulator[season][batsman]["balls"] = 1;
              accumulator[season][batsman]["total_runs"] = Number(batsman_runs);
            }
          } else {
            accumulator[season] = {};
            accumulator[season][batsman] = {};
            accumulator[season][batsman]["balls"] = 1;
            accumulator[season][batsman]["total_runs"] = Number(batsman_runs);
          }
        });
      return accumulator;
    },
    {}
  );
  const strikeRatePerSeason = Object.keys(runsAndBallsPerSeason).reduce(
    (accumulator, season) => {
      Object.keys(runsAndBallsPerSeason[season]).forEach((batsman) => {
        let runs = runsAndBallsPerSeason[season][batsman]["total_runs"];
        let balls = runsAndBallsPerSeason[season][batsman]["balls"];
        var strikeRate = ((runs / balls) * 100).toFixed(2);
        if (accumulator[season]) {
          accumulator[season][batsman] = strikeRate;
        } else {
          accumulator[season] = {};
          accumulator[season][batsman] = strikeRate;
        }
      });
      return accumulator;
    },
    {}
  );
  return strikeRatePerSeason;
};
/**
 * The function accepts the ball to ball analysis in the ipl project.
 * Returns an object of which player name as property and number of he was dismissed in its value position.
 * @param {Array} deliveries - The array of objects.
 * @returns {Object} - Returns an object.
 */

const highestNumberOfTimesPlayerDismissed = (deliveries) => {
  const playerDismissed = deliveries.reduce(
    (accumulator, { player_dismissed, bowler }) => {
      if (player_dismissed) {
        if (accumulator[player_dismissed] !== undefined) {
          if (accumulator[player_dismissed][bowler] !== undefined) {
            accumulator[player_dismissed][bowler] += 1;
          } else {
            accumulator[player_dismissed][bowler] = 1;
          }
        } else {
          accumulator[player_dismissed] = {};
          accumulator[player_dismissed][bowler] = 1;
        }
      }
      return accumulator;
    },
    {}
  );
  const highestNumber = Object.keys(playerDismissed).reduce(
    (accumulator, player_dismissed) => {
      let player = Object.keys(playerDismissed[player_dismissed]).reduce(
        (a, b) =>
          playerDismissed[player_dismissed][a] >
          playerDismissed[player_dismissed][b]
            ? a
            : b
      );
      accumulator[player_dismissed] = {
        [player]: playerDismissed[player_dismissed][player],
      };
      return accumulator;
    },
    {}
  );
  return highestNumber;
};
/**
 * The function accepts the ball to ball analysis in the ipl project.
 * Returns an object of which player name as property and his economy in super overs as value to its property.
 * @param {Array} deliveries - The array of objects.
 * @returns {Object} - Returns an object
 */
const bestEconomicalInSuperOvers = (deliveries) => {
  const ballsAndRuns = deliveries
    .filter(({ is_super_over }) => is_super_over == "1")
    .reduce((accumulator, { bowler, total_runs }) => {
      if (accumulator[bowler]) {
        accumulator[bowler]["runs"] += Number(total_runs);
        accumulator[bowler]["balls"] += 1;
      } else {
        accumulator[bowler] = {};
        accumulator[bowler]["runs"] = Number(total_runs);
        accumulator[bowler]["balls"] = 1;
      }
      return accumulator;
    }, {});

  const economy = Object.keys(ballsAndRuns).reduce((accumulator, player) => {
    let runsConceded = ballsAndRuns[player]["runs"];
    let overs = ballsAndRuns[player]["balls"] / 6;
    let economy = runsConceded / overs;
    accumulator[player] = economy.toFixed(2);
    return accumulator;
  }, {});
  const bestBowler = Object.keys(economy).sort(
    (a, b) => economy[a] - economy[b]
  )[0];
  const bestBowlerInSuperOvers = {};
  bestBowlerInSuperOvers[bestBowler] = economy[bestBowler];
  return bestBowlerInSuperOvers;
};
/**
 * The function accepts the details about ipl matches and ball to ball analysis.
 * Returns an object of which year takes the property and another object as value to that property.
 * The value object contains '4', '6' as property and number of fours, sixes as values to it.
 * @param {Array} matches - The array of objects.
 * @param {Array} deliveries - The array of objects.
 * @returns {Object} - Returns the object.
 */

const numberOfBoundariesDhoniScored = (matches, deliveries) => {
  const dhoni = matches.reduce((accumulator, { season, id }) => {
    deliveries
      .filter(
        ({ match_id, batsman, batsman_runs }) =>
          match_id === id &&
          batsman == "MS Dhoni" &&
          (batsman_runs == "4" || batsman_runs == "6")
      )
      .map(({ batsman_runs }) => {
        if (accumulator[season]) {
          if (accumulator[season][batsman_runs]) {
            accumulator[season][batsman_runs] += 1;
          } else {
            accumulator[season][batsman_runs] = {};
            accumulator[season][batsman_runs] = 1;
          }
        } else {
          accumulator[season] = {};
          accumulator[season][batsman_runs] = {};
          accumulator[season][batsman_runs] = 1;
        }
      });
    return accumulator;
  }, {});
  return dhoni;
};

const boundariesByKohli = (matches, deliveries, year = 2010) => {
  const boundaries = matches
    .filter(({ season }) => season == year)
    .reduce((accumulator, { id }) => {
      deliveries
        .filter(
          ({ match_id, batsman, batsman_runs }) =>
            match_id === id &&
            batsman == "V Kohli" &&
            (batsman_runs == "4" || batsman_runs == "6")
        )
        .map(({ batsman_runs }) => {
          if (accumulator[batsman_runs]) {
            accumulator[batsman_runs] += 1;
          } else {
            accumulator[batsman_runs] = 1;
          }
        });
      return accumulator;
    }, {});
  return boundaries;
};

const playerOfTheMatch = (matches, deliveries, year = 2010) => {
  const player = matches
    .filter(({ season }) => season == year)
    .reduce((accumulator, { id, player_of_match }) => {
      deliveries
        .filter(
          ({ match_id, batsman }) =>
            match_id === id && batsman === player_of_match
        )
        .map(({ batsman, batsman_runs }) => {
          if (accumulator[id]) {
            if (accumulator[id][batsman]) {
              accumulator[id][batsman]["runs"] += Number(batsman_runs);
            } else {
              accumulator[id][batsman] = {};
              accumulator[id][batsman]["runs"] = Number(batsman_runs);
            }
          } else {
            accumulator[id] = {};
            accumulator[id][batsman] = {};
            accumulator[id][batsman]["runs"] = Number(batsman_runs);
          }
        });

      return accumulator;
    }, {});
  return player;
};

const purpleCapHolder = (matches, deliveries) => {
  const bowlerNames = matches.reduce((bowlerName, { id, season }) => {
    deliveries
      .filter(({ match_id }) => match_id === id)
      .forEach(({ bowler, player_dismissed }) => {
        if (player_dismissed) {
          if (bowlerName[season]) {
            if (bowlerName[season][bowler]) {
              bowlerName[season][bowler] += 1;
            } else {
              bowlerName[season][bowler] = 1;
            }
          } else {
            bowlerName[season] = {};
            bowlerName[season][bowler] = 1;
          }
        }
      });
    return bowlerName;
  }, {});
  const mostWickets = Object.keys(bowlerNames).reduce((purpleCap, season) => {
    const player = Object.keys(bowlerNames[season]).reduce((a, b) =>
      bowlerNames[season][a] < bowlerNames[season][b] ? b : a
    );
    purpleCap[season] = { [player]: bowlerNames[season][player] };
    return purpleCap;
  }, {});
  return mostWickets;
};

const mostWinsAgainstMumbai = (matches) => {
  const teams = matches
    .filter(
      ({ team1, team2, winner }) =>
        (team1 == "Mumbai Indians" && winner !== "Mumbai Indians") ||
        (team2 == "Mumbai Indians" && winner !== "Mumbai Indians")
    )
    .reduce((team, { winner }) => {
      if (team[winner]) {
        team[winner] += 1;
      } else {
        team[winner] = 1;
      }
      return team;
    }, {});
  return teams;
};

const orangeCapHolder = (matches, deliveries) => {
  const batsmanNames = matches.reduce((batsmanName, { id, season }) => {
    deliveries
      .filter(({ match_id }) => match_id === id)
      .forEach(({ batsman, batsman_runs }) => {
        if (batsmanName[season]) {
          if (batsmanName[season][batsman]) {
            batsmanName[season][batsman] += Number(batsman_runs);
          } else {
            batsmanName[season][batsman] = Number(batsman_runs);
          }
        } else {
          batsmanName[season] = {};
          batsmanName[season][batsman] = Number(batsman_runs);
        }
      });
    return batsmanName;
  }, {});
  const mostRuns = Object.keys(batsmanNames).reduce((orangeCap, season) => {
    const player = Object.keys(batsmanNames[season]).reduce((a, b) =>
      batsmanNames[season][a] < batsmanNames[season][b] ? b : a
    );
    orangeCap[season] = { [player]: batsmanNames[season][player] };
    return orangeCap;
  }, {});
  return mostRuns;
};

const dhoniVsMalinga = (matches, deliveries) => {
  const boundaries = matches.reduce((boundary, { id, season }) => {
    deliveries
      .filter(
        ({ match_id, batsman, bowler, batsman_runs }) =>
          match_id === id &&
          batsman == "MS Dhoni" &&
          bowler == "SL Malinga" &&
          (batsman_runs == "6" || batsman_runs == "4")
      )
      .map(({ batsman_runs }) => {
        if (boundary[season]) {
          if (boundary[season][batsman_runs]) {
            boundary[season][batsman_runs] += 1;
          } else {
            boundary[season][batsman_runs] = 1;
          }
        } else {
          boundary[season] = {};
          boundary[season][batsman_runs] = 1;
        }
      });
    return boundary;
  }, {});
  return boundaries;
};

const rainaVsBangalore = (matches, deliveries) => {
  const rainaRuns = matches
    .filter(
      ({ team1, team2 }) =>
        team1 == "Royal Challengers Bangalore" ||
        team2 == "Royal Challengers Bangalore"
    )
    .reduce((runs, { id, season }) => {
      deliveries
        .filter(
          ({ match_id, batsman }) => match_id === id && batsman == "SK Raina"
        )
        .map(({ batsman_runs }) => {
          if (runs[season]) {
            runs[season] += Number(batsman_runs);
          } else {
            runs[season] = Number(batsman_runs);
          }
        });
      return runs;
    }, {});
  return rainaRuns;
};

const partnershipBwnGayleAndKohli = (matches, deliveries) => {
  const partnership = matches.reduce((runs, { id, season }) => {
    deliveries
      .filter(
        ({ match_id, batsman, non_striker }) =>
          (match_id === id &&
            batsman == "CH Gayle" &&
            non_striker == "V kohli") ||
          (match_id === id && batsman == "V Kohli" && non_striker == "CH Gayle")
      )
      .map(({ batsman_runs, match_id }) => {
        if (runs[season]) {
          if (runs[season][match_id]) {
            runs[season][match_id] += Number(batsman_runs);
          } else {
            runs[season][match_id] = Number(batsman_runs);
          }
        } else {
          runs[season] = {};
          runs[season][match_id] = Number(batsman_runs);
        }
      });
    return runs;
  }, {});
  return partnership;
};
module.exports = {
  extraRunsIn2016,
  matchesWonPerTeam,
  matchesPerYear,
  mostEconomicalBowlers,
  teamWinningBothTossAndMatch,
  playerOfTheMatchPerSeason,
  strikeRate,
  highestNumberOfTimesPlayerDismissed,
  bestEconomicalInSuperOvers,
  numberOfBoundariesDhoniScored,
  boundariesByKohli,
  playerOfTheMatch,
  purpleCapHolder,
  mostWinsAgainstMumbai,
  orangeCapHolder,
  dhoniVsMalinga,
  rainaVsBangalore,
  partnershipBwnGayleAndKohli,
};
